<?php
?>
      <div class="row">
                        <div class="section-title text-center">
                            <h3>Личный Кабинет</h3>
                            <p>Вход в личный кабинет</p>
                        </div>
                    </div>
                    <div class="row">
                    </div><!-- /.row -->
                    <div class="row">
                        <div class="col-md-7">
                            <div class="custom-tab">
                        
                                <ul class="nav nav-tabs nav-justified" role="tablist">
                                    <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Вход в личный кабинет</a></li>
                                    <li><a href="#tab-2" role="tab" data-toggle="tab">Напоминание пароля</a></li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane active" id="tab-1">
                                       <form name="sentMessage" id="contactForm" novalidate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Имя аккаунта *" id="name" required data-validation-required-message="Желаемое имя аккаунта">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Пароль *" id="pass" required data-validation-required-message="Ваш пароль от аккаунта">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12 text-center">
                                        <button type="submit" class="btn btn-primary"> Войти </button><br>
                                    <div id="success"></div>
                                    </div>
                                </div>
                            </form>
                                     </div>


                                    <div class="tab-pane" id="tab-2">
                                        <form name="sentMessage" id="contactForm" novalidate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Почтовый ящик *" id="name" required data-validation-required-message="E-mail на который зарегистрирован аккаунт">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                         <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Имя аккаунта *" id="name" required data-validation-required-message="Имя аккаунта для восстановления">
                                            <p class="help-block text-danger"></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12 text-center">
                                        <div id="success"></div>
                                        <button type="submit" class="btn btn-primary">Выслать пароль</button>
                                    </div>
                                </div>
                            </form>
                                    </div>

                                </div><!-- /.tab-content -->

                            </div>
                        </div>
                    </div>