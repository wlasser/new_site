$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var name = document.getElementById('name').value;
            var pass = document.getElementById('pass').value;
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $.ajax({
                url: "includes/do.php",
                type: "POST",
                data: {
                    what: "login",
                    name: name,
                    pass: pass
                },
                cache: false,
                   beforeSend: function(){
                $('#success').html('<img id="imgcode" src="img/other/preload.gif">');
            },
                success: function(data) {
                    // Success message
                    //alert (data);
                    if (data=='0')
                    {
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                        $('#success > .alert-danger')
                        .append("<strong>Неверный логин или пароль! </strong>");
                        $('#success > .alert-danger')
                        .append('</div>');
                    }
                    else
                        $('#lk').html(data)
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
