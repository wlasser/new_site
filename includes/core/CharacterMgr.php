<?php
class CharacterMgr extends AccountMgr
{
	
    
        public function GetOnline($realm)
        {
            $conn = $this->connect($this->GetCharTableFromRealmId($realm));
            $sth=$conn->prepare(select_statements::CHAR_SEL_ONLINE);
            $sth->execute();
		
            $num = $sth->rowCount();
            
	return $num;  
        }
    
	public function GetCharsCount($realm, $acc_id)
	{
		$conn = $this->connect($this->GetCharTableFromRealmId($realm));
		$sth=$conn->prepare(select_statements::CHAR_SEL_ALL_ACC_ID);
		$execute_param = array($acc_id);
		$sth->execute($execute_param);
		
			$num = $sth->rowCount();
		return $num;
	} 

	public function GetCharNamesForAcc($realm, $accid)
	{
		$test=array();
		
		$conn = $this->connect($this->GetCharTableFromRealmId($realm));
		$sth = $conn->prepare(select_statements::CHAR_SEL_ALL_CHAR_ACCID);
		$execute_params =array($accid) ;
		$sth->execute($execute_params);
		
			while ($row = $sth->fetch(PDO::FETCH_ASSOC))
				$test[$row['guid']] = $row['name'];
                        
	return $test;
	}

        public function GetCharGuidFromName($name)
        {
            $conn = $this->GetCharDb();
            
            $sth = $conn->prepare(select_statements::CHAR_SEL_GUID);
            
            $execute_params =array($name);
            $sth->execute($execute_params);
            
            while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                    $guid = $row['guid'];
            
        return $guid;    
        }
        
        public function GetCharNameFromGuid($guid)
        {
            $conn = $this->GetCharDb();
            $sth = $conn->prepare(select_statements::CHAR_SEL_NAME);
            $execute_params =array($guid) ;
            $sth->execute($execute_params);
            
            while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                    $name = $row['name'];
            
        return $name;   
        }
        
        public function GetCharHome($name)
        {
            $race = $this->GetCharRace($name);
            
            switch ($race){
                case Race::BE:
                case Race::GOBLIN:
                case Race::MONK_H:
                case Race::ORC:
                case Race::TAUREN:
                case Race::TROLL:
                case Race::UNDEAD:
                    $location = "Orgrimmar";
                    break;
                case Race::DRENEI:
                case Race::DWARF:
                case Race::GNOME:
                case Race::HUMAN:
                case Race::MONK_A:
                case Race::NE:
                case Race::WORGEN:
                    $location = "Stormwind";
                    break;
                default:
                    $location = "Siegeof";
                    break;
            }
        return $location;    
        }
        
        public function GetCharRace($name)
        {
            $conn = $this->GetCharDb();
            
            $sth = $conn->prepare(select_statements::CHAR_SEL_RACE);
            $execute_params =array($name) ;
            $sth->execute($execute_params);
            
            while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                    $race = $row['race'];
            
        return $race;
        }
        
        public function GetCharClass($name)
        {
            $conn = $this->GetCharDb();
            
            $sth = $conn->prepare(select_statements::CHAR_SEL_CLASS);
            $execute_params =array($name) ;
            $sth->execute($execute_params);
            
            while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                    $class = $row['class'];
            
        return $class;
        }
        
        public function GetCharLvl($name)
        {
             $conn = $this->GetCharDb();
            
            $sth = $conn->prepare(select_statements::CHAR_SEL_CLASS); //this is doesn't matter
            $execute_params =array($name) ;
            $sth->execute($execute_params);
            
            while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                    $lvl = $row['level'];
            
        return $lvl;
        }
    
    public function GetFirstCharacter($accid)
    {
        $conn = $this->GetCharDb();
            
        $sth = $conn->prepare(select_statements::CHAR_SEL_MIN); //this is doesn't matter
        $execute_params =array($accid) ;
        $sth->execute($execute_params);
          
        while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                $char = $row['MIN(guid)'];
            
    return $char;
    }
        
    public function SetCharacterCookie($accid)
    {
        $guid = $this->GetFirstCharacter($accid);
        $name = $this->GetCharNameFromGuid($guid);
        
        setcookie("char", $name, time() + 3600, '/');
    }
}
?>