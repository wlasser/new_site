<?php
	interface BonusTypes
	{
		const BONUS_TYPE_VOTE = 1;
		const BONUS_TYPE_DONOR = 2;
		const MAX_BONUS_TYPES = 2;
	}
	
	 interface Faction 
	{
		const ALLIANCE = 0;
		const HORDE = 1;
	}

	 interface Race
	{
		const WORGEN = 22 ;
		const HUMAN = 1;
		const DRENEI = 11;
		const GNOME = 7;
		const DWARF = 3;
		const NE = 4;
		const ORC = 2;
		const UNDEAD = 5;
		const BE = 10;
		const TAUREN = 6;
		const TROLL = 8;
		const GOBLIN = 9;
                const MONK_A = 25;
                const MONK_H = 26;
	   // etc.
	} 
	
	 interface Classes
	{
		const WARRIOR = 1;
		const PALADIN =2;
		const HUNTER =3;
		const ROGUE =4;
		const PRIEST =5;
		const DK =6;
		const SHAMAN =7;
		const MAGE =8;
		const WARLOCK =9;
		const DRUID =11;
	}
	
	 interface Times
	{
		const MIN_IN_HOUR = 60;
		const HOURS_IN_DAY = 24;
		const SEC_IN_HOUR = 3600;
		const SEC_IN_DAY = 86400;
		const CACHE_REFRESH = 600;
		const SEC_IN_15_DAY = 2592000;
		const MAGIC_TIME_FOR_COOKIE = 9000;
	}
	 
	 interface Misc
	{
			const MAX_CHARACTER_PER_REALM = 10;
			const MAX_CHARACTER_CLASSES = 10;
	}	
	
	interface Addition
	{
		const WORGEN_MIN_LVL_FOR_UNSTUCK = 30;
		const DK_MIN_LVL_FOR_UNSTUCK = 78;
		const MIN_LVL_FOR_UNSTUCK = 20; 
                const MIN_LVL_PANDA = 30;
	}
?>