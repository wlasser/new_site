<?php
class ServiceMgr extends CharacterMgr
{
    
    public function ViewShop($type, $page=0)
    {
        $conn = $this->GetCmsDb();
        $min_limit = $this->GetMinLimit($page);
        $max_limit=$this->GetItemsPerPage();
       // echo $min_limit,"<br>",$max_limit;
        $sth = $conn->prepare(select_statements::CMS_SELECT_TYPE_MIN);     
//        $execute_params =array($type,) ; //, $min_limit, $max_limit
        $sth->bindValue(1, $type, PDO::PARAM_STR);
	$sth->bindValue(2, (int)$min_limit, PDO::PARAM_INT);
	$sth->bindValue(3, (int)$max_limit, PDO::PARAM_INT);
        $sth->execute($execute_params);
                   
        $sth_total = $conn->prepare(select_statements::CMS_SELECT_TYPE_ALL);
        $execute_params = array($type);
        $sth_total->execute($execute_params);
        
        $total_page = round($sth_total->rowCount()/$max_limit);
        echo '<table>';
        echo '<tr><td></td><td></td><td></td></tr>';
            while ($row = $sth->fetch(PDO::FETCH_ASSOC)){
            echo '<tr>';
                printf('<td><img src="http://wow.zamimg.com/images/wow/icons/large/%s.jpg" width="30" height="30"></td><td><a href="test" rel="domain=ru item=%s">%s</a></td><td>%s</td>',$row['icon'],$row['whid'],$row['caption'],$row['price']);
                echo "\n</tr>";
            }
         echo '</table>';
       // $shop_id = $this->GetShopItemFromName($type);
//        echo $shop_id;
        if ($page>=$total_page-1)
            $next_page=0;
	else
            $next_page = $page+1;
	if ($total_page==1)
            echo '<br>';
	else
            echo '<br><a href="#" class="nextPage" id="',$type,'" data-id="'.$next_page.'"><img src="img/lk/more.png" /></a>';
        
        
    }
      
    
    public function GetShopItemFromName($type)
    {
	switch ($type){
            case 'item':
		$id = 1;
		break;
            case 'weapon':
		$id = 2;
		break;
            case 'misc':
		$id = 3;
		break;
            case 'mount':
		$id = 4;	
		break;
	}
		
	return $id;
	}
        
    public function RepairCharacter($char_guid=0, $realm=1)
    {
        //$characterMgr= new CharacterMgr();
        $soapMgr = new SoapMgr();
        
       // echo '123';
       if (!$_COOKIE['char'])
            $contentMgr->GotoPage('index.php');
		
       //var_dump($characterMgr);
        $char_name=$this->GetCharNameFromGuid($char_guid);	
//        
       // echo $char_name;
//				
	$location = $this->GetCharHome($char_name);
	$apply = $this->UnstuckLog($char_guid, $realm);
//        
	if (!$apply)
           die('Доступно лишь 1 раз в 30 минут для каждого персонажа');
//        //$characterMgr->GetCharRace($char_name), $characterMgr->GetCharClass($char_name), $characterMgr->GetCharLvl($char_name)
	$race = $this->GetCharRace($char_name);
        $class = $this->GetCharClass($char_name);
        $lvl = $this->GetCharLvl($char_name);
        $message = $this->CheckForUnstuckAddition($race,$class,$lvl);
//	
        if ($message)
            die ($message);
//
	$command_tele = 'tele name '.$char_name.' '.$location;
	$command_revive = 'revive '.$char_name;

	//ConfigMgr()->CleanAuraForCharGuid($char_guid);

	$soap_command = $soapMgr->ExecuteSoapCommand($command_tele);
	$soapMgr->ExecuteSoapCommand($command_revive);
				
	if($soap_command['sent'])
            echo 'Персонаж был отправлен в '.$location.'. Приятной игры!';
	else 
            echo 'В данный момент услуга не доступна. Если вы видите это сообщение, сообщите о нем на нашем форуме. '.$soap_command['message']; 
    }
    
    private function CheckForUnstuckAddition($race=0, $class=0, $level_req=0)
    {
		$message = NULL;
		//echo '555';
                
                
                if ($race==Race::MONK_A || $race==Race::MONK_H && $level_req<Addition::MIN_LVL_PANDA)
                    $message = "Сорянчик, но пандаренов можно вытащить только после 30-го уровня";
				
		if ($class== Classes::DK && $level_req<Addition::DK_MIN_LVL_FOR_UNSTUCK)
			$message = 'Извините, но Рыцаря смерти можно вернуть домой только с '.Addition::DK_MIN_LVL_FOR_UNSTUCK.' уровня.';
				
		if ($level_req<Addition::MIN_LVL_FOR_UNSTUCK )
			$message = 'Использовать данную услугу можно только с '.Addition::MIN_LVL_FOR_UNSTUCK .' уровня.';

    return $message;
    }
    
    public function UnstuckLog($char_guid, $realmid=1)
    {
        $conn = $this->GetCmsDb();
	$sth = $conn->prepare(select_statements::CMS_SEL_CHGUID_UNSTUCK_LOG);
	//$execute_params = array($char_guid, $realmid);
	$sth->bindValue(1, (int)$char_guid, PDO::PARAM_INT);
        $sth->bindValue(2, (int)$realmid, PDO::PARAM_INT);
        $sth->execute();
    	$time=time();
       
	if (!$sth->rowCount())
            {
            
            //echo $char_guid."realm".$realmid;
            //echo "badum!";
		$sth = $conn->prepare(insert_statements::CMS_INS_UNSTUCK_LOG);
		$execute_params = array($time,$char_guid,$realmid);
		$sth->execute($execute_params);
                
            return TRUE;
            }
	    else 
	    {
	     while ($row=$sth->fetch(PDO::FETCH_ASSOC))
		$time_from_table = $row['time'];
//							
//  
             //echo $time_from_table;
			$diff = $time - $time_from_table;
             //echo "<br>".$diff;
			
		        $time_for_rest = ($this->unstuck_reset/Times::MIN_IN_HOUR)-round($diff/Times::MIN_IN_HOUR);
//							
			$message = ($time_for_rest<0)?"Время до следующего использования 30 минут <br>":"Время до следующего использования : ".$time_for_rest." минут<br>";					
//					
		        echo $message;
//						
			if ($diff >= $this->unstuck_reset)
                        {							 
			    $sth = $conn->prepare(update_statements::CMS_UPD_UNSTUCK_LOG);
			    $execute_params = array($time,$char_guid,$realmid);
                            $sth->execute($execute_params);
			return TRUE;
			}
							
            return 0;
            }
    }
    
   
}
