<?php
// accounts system. loading account data and other...

class AccountMgr extends ConfigMgr
{



	public function LoginCheck($user, $pass, $capcha=0) 
	{
		$conn = $this->GetAuthDb();
		$pass = $this->SafeString($this->ShaPass($user,$pass));
		$user = $this->SafeString($user); 
					
		$sth = $conn->prepare(select_statements::AUTH_SEL_ALL_USER_PASS);
		$execute_params =array($user,$pass) ;
		$sth->execute($execute_params);

			if (!$sth->rowCount())
				return false;
			else			
				return true; 
			
	}

	public function GetAccountIdFromName($name)
	{
		$conn = $this->GetAuthDb();
		$sth = $conn->prepare(select_statements::AUTH_SEL_ID_ACC_USERNAME);
		$execute_params =array($name) ;
		$sth->execute($execute_params);
			
			while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$id =  $row['id'];
						
	return $id;
	}

	public function GetPrimaryRealm($accid)
	{
			$conn = $this->GetAuthDb();
			$sth = $conn->prepare(select_statements::AUTH_SEL_REALMCHARS_ACCTID);
			$execute_params =array($accid) ;
			$sth->execute($execute_params);
				
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$realm = $row['realmid'];

	return $realm;
	}

	public function CompleteRegister($user, $pass, $pass2, $email, $capcha=0) // fix it latter
	{
		$user = $this->SafeString($user);
		$pass = $this->SafeString($pass);
		$pass2 = $this->SafeString($pass2);
		$email = $this->SafeString($email);
                
		//$capcha = $this->SafeString($capcha);

		//$this->CheckCapcha($capcha);

		$this->CheckForUnsupportChars($user);
		$this->CheckForUnsupportChars($pass);
		$this->CheckForUnsupportChars($email,1);
                
                
		$this->CheckLength($user);
		$this->CheckLength($user);
		$this->CheckLength($pass);
		$this->CheckAccountExist($user);	
		$this->CheckEmailExist($email);	
                if ($capcha){
                    $capcha = $this->SafeString($capcha);
                    $this->CheckForUnsupportChars($capcha);
                    $this->CheckCapcha($capcha);
                }
		$addon = $this->addon;
		
		$conn = $this->GetAuthDb();
		
			$sth = $conn->prepare(insert_statements::AUTH_INS_ACC_PASS);
			$execute_params = array($user, $user, $pass, $email, $addon); //user+user needed for sql query set shapass
			$sth->execute($execute_params);
					
			//$this->RewardRegister();
			return true;
				
	}

	public function CheckAccountExist($user)
	{
		$conn = $this->GetAuthDb();	
		$sth = $conn->prepare(select_statements::AUTH_SEL_ALL_USERNAME);
		$execute_params =array($user) ;
		$sth->execute($execute_params);
	
			if ($sth->rowCount())
				die (' Введеный аккаунт уже существует.');
	}

	public function CheckEmailExist($email)
	{
		$conn = $this->GetAuthDb();
		$sth = $conn->prepare(select_statements::AUTH_SEL_ALL_EMAIL);
		$execute_params=array($email);
		$sth->execute($execute_params);
	
			if ($sth->rowCount())
				die (' Введеный e-mail уже используется.');	
	}
        
        public  function CheckCapcha($capcha)
        {
            if ($_COOKIE['randomo']!=$capcha){
                die('Неверная капча!');
                
            }
        }
}

?>