<?php
class BonusMgr extends ConfigMgr
{
    
    public function GetBonus($accid)
    {
        $conn = $this->GetCmsDb();
            
        $sth = $conn->prepare(select_statements::CMS_SEL_BONUS);
        $execute_params =array($accid) ;
        $sth->execute($execute_params);
        
        if (!$sth->rowCount())
            return -1;
        
        while ($row = $sth->fetch(PDO::FETCH_ASSOC))
                $bonus = $row['bonus'];
            
    return $bonus;
    }
    
    public function ModifyBonus($accid, $value)
    {
        $conn = $this->GetCmsDb();
        
        if ($this->GetBonus($accid)==-1){
            if ($value<0)
                return 'Увы!';
            
            $sth=$conn->prepare(insert_statements::CMS_INS_BONUS);
            $execute_params =array($accid,$value,1);
            $sth->execute($execute_params);
        }
        
        if ($value<0){
            
            $bonus = $this->GetBonus($accid);
            
            if ($bonus > abs($value))
                $new_bonus = $bonus+$value;
            else
                return 'Недостаточно бонусов';
        
        }
        elseif($value>0){
            $new_bonus=$bonus+$value;
        }
        
        $sth->prepare(update_statements::CMS_UPD_BONUS);
        $execute_params=array($new_bonus,$accid);
        $sth->execute($execute_params);
        
        return 'Сделано.';
                
    }
    
    
}

