<?php
class SoapMgr extends ConfigMgr
{
    public function ExecuteSoapCommand($command)
    {
	$realm = 1;
	$port = $this->GetSoapPort($realm);
	$server =  $this->soap_server;
	//echo 'testsadasdsad';
	$location = $server.":".$port;
        
	try{
            if ($realm){
                $server = array(
		"location" =>$location, 
		"uri"   => "urn:TC",
		"style" => SOAP_RPC,
		"login" => $this->soap_account,
		"password" => $this->soap_password);
		}

			$client = new SoapClient(NULL,$server);
			
			$result = $client->executeCommand(new SoapParam($command, "command"));
    		}
			catch(Exception $e)
			{
				return array('sent' => false, 'message' => $e->getMessage());
			}

	return array('sent' => true, 'message' => $result);
	}
}
