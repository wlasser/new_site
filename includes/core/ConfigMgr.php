<?php
class ConfigMgr
{
	public $host, $username, $pass, $root_dir, $port;

	public $account_base, $char_tables, $cms_base;

	public $addon, $items_per_page;

	public $min_reg, $max_reg;

        public $soap_server, $soap_account, $soap_password;
        
        public $unstuck_reset;
        
	public function __construct()
	{
		// i think this can be do something else ? -->

		include 'config.php';
		require_once 'defines.php';
		require_once 'prep_statements.php';


		$this->host=$server;
		$this->port =$port;
		$this->username=$user;
		$this->pass=$pass;
		$this->account_base=$account;
		$this->cms_base =$cms;
		$this->addon = $addon;
		$this->min_reg = $min_reg;
		$this->max_reg = $max_reg;
                $this->items_per_page = $items_per_page;
                $this->soap_server = $soap_server;
                $this->soap_account = $soap_account;
                $this->soap_password = $soap_password;
                $this->unstuck_reset=$unstuck_reset;
               

	}

	// connect function. used elsewhere...

	public function connect($base)
	{
		$conn = new PDO('mysql:host='.$this->host.';port='.$this->port.';dbname='.$base, $this->username, $this->pass);// or die ("Error");
		$conn->query(other_statements::SET_UTF_CHARSET);
			
	return $conn;
	}

	public function GetAuthDb()
	{
		$conn = $this->connect($this->account_base); 
		
	return $conn;
	}
        
        public function GetCmsDb()
        {
            echo $this->cms_base;
            $conn = $this->connect($this->cms_base);
            
        return $conn;
        }
        
        public function GetItemsPerPage()
        {
            return $this->items_per_page;
        }


        public function GetCharDb()
        {
            $conn = $this->connect($this->GetCharTableFromRealmId(1));
         
        return $conn;
        }

        
        public function GetMinLimit($page)
	{
		$page = (int)$page;
                
                if (!$page ){	
                    $min_limit =0;
                }
		$min_limit = ($page*5)*2;
				
	return $min_limit;
	}

	public function GetCharTableFromRealmId($realm)
	{
		$conn = $this->GetAuthDb();
		$sth = $conn->prepare(select_statements::AUTH_SEL_CHAR_TABLE_REALMLIST_ID);
		$execute_params =array($realm);

		$sth->execute($execute_params);
			while ($row = $sth->fetch(PDO::FETCH_ASSOC))
					$table =  $row['char_table'];
				
	return $table;
	}

	public function SafeString($string) 
	{
		$string = stripslashes($string);
		$string = strip_tags($string);
		
	return $string;
	}

	public function ShaPass($user,$pass)
	{
		$user = strtoupper($user);
		$pass = strtoupper($pass);
		
	return SHA1($user.':'.$pass);
	}	

          public function GetSoapPort($realm)
        {
            $conn = $this->GetAuthDb();
        
            $sth = $conn->prepare(select_statements::AUTH_SEL_SOAP_PORT);
            $execute_params =array($realm);
            
            $sth->execute($execute_params);
		while ($row = $sth->fetch(PDO::FETCH_ASSOC))
			$soap_port =  $row['soap_port'];
				
	return $soap_port;
        }
    
        
	public function CheckForUnsupportChars($string ,$email=0)
	{
		if (!$email)
		{
			if (!preg_match("/^[a-zA-Z0-9_]+$/", $string))
				die (' Вы ввели недопустимые символы. Попробуйте снова. Латинские буквы и цифры от 0 до 9');
		}
		else 
		{	
			if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/",$string))
				die (' Недопустимый e-mail.');	
		}

	return true;
	}

	// there is many questions about it. idk how do it properly...

	public function CheckLength($string)
	{
		$message_more = ' Слишком много символов. Попробуйте снова.';
		$message_many = ' Слишком мало символов. Попробуйте снова.';
	
			if (strlen($string) >= $this->max_reg )
				die ($message_more);
			elseif (strlen($string) <= $this->min_reg)
				die ($message_many);
			else
				return true;
	}

}
?>