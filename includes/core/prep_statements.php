<?php
// prep_statements
	interface other_statements
	{
		const SET_UTF_CHARSET = "SET NAMES utf8";
	}

	interface select_statements
	{
                const CMS_SEL_BONUS = "SELECT * FROM bonuses WHERE accountid=?";
		// accountMgr
		const AUTH_SEL_ALL_USER_PASS = "SELECT * FROM `account` WHERE `username` = ? AND `sha_pass_hash` = ?";
		const AUTH_SEL_ID_ACC_USERNAME = "SELECT id FROM account where username = ?";
		const AUTH_SEL_REALMCHARS_ACCTID = "SELECT MAX(numchars),realmid FROM realmcharacters WHERE acctid=?";
		const AUTH_SEL_ALL_USERNAME ="SELECT * FROM account WHERE username=?";
		const AUTH_SEL_ALL_EMAIL = "SELECT * FROM account WHERE email=?";
                
		// configMgr
		const AUTH_SEL_CHAR_TABLE_REALMLIST_ID = "SELECT char_table FROM realmlist WHERE id = ?";
                const AUTH_SEL_SOAP_PORT = "SELECT soap_port FROM realmlist WHERE id=?";
                
		// characterMgr
		const CHAR_SEL_ALL_ACC_ID = "SELECT * FROM characters WHERE account = ?";
		const CHAR_SEL_ALL_CHAR_ACCID = "SELECT * FROM characters WHERE account = ?";
                const CHAR_SEL_ONLINE = "SELECT * FROM characters WHERE online=1";
                const CHAR_SEL_GUID = "SELECT * FROM characters WHERE name=?";
                const CHAR_SEL_NAME = "SELECT * FROM characters WHERE guid=?";
                const CHAR_SEL_RACE = "SELECT * FROM characters WHERE name=?";
                const CHAR_SEL_CLASS = "SELECT * FROM characters WHERE name=?";
                const CHAR_SEL_MIN = "SELECT MIN(guid) from characters WHERE account=?";
                // shop
                const CMS_SELECT_TYPE_MIN = "SELECT * FROM item_mall WHERE type = ? ORDER BY id LIMIT ?,?";//  
                const CMS_SELECT_TYPE_ALL = "SELECT * FROM item_mall WHERE type = ?";
                
                //serviceMgr
                const CMS_SEL_CHGUID_UNSTUCK_LOG  = "SELECT * FROM unstuck_log WHERE chguid=? AND realm=?";
                
             
	}

	interface insert_statements
	{
            //bonusmgr
            const CMS_INS_BONUS = "INSERT INTO bonuses VALUES (?,?,?)";
            // serviceMgr
            const CMS_INS_UNSTUCK_LOG = "INSERT INTO unstuck_log VALUES ('',?,?,?)";
		// acountMgr
		//$user, $user, $pass, $email, $addon
	    const AUTH_INS_ACC_PASS = "INSERT INTO account (username, sha_pass_hash, email, expansion) VALUES (?, SHA1(CONCAT(UPPER(?),':',UPPER(?))), ?, ?)";
	}
        
        interface update_statements
        {
            const CMS_UPD_UNSTUCK_LOG = "UPDATE unstuck_log SET time = ? where chguid = ? AND realm = ?";
            const CMS_UPD_BONUS = "UPDATE bonuses SET bonus=? WHERE accountid=?";
        }