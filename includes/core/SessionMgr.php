<?php
class SessionMgr extends AccountMgr
{

	public function __construct()
	{
	}

	public function CreateSession($user)
	{
		session_start();

		if (empty($_SESSION['user']))
			$_SESSION['user']=$user;
	}

	public function GetSession()
	{
		if (isset($_SESSION['user']))
			return $_SESSION['user'];

		return NULL;
	}

	public function KillSession()
	{
		session_start();
		session_unset();
		session_destroy();
	}
}
?>