<?php

require_once 'core/defines.php';
require_once 'core/prep_statements.php';

require_once 'core/ConfigMgr.php';
require_once 'core/AccountMgr.php';
require_once 'core/SessionMgr.php';
require_once 'core/ContentMgr.php';
require_once 'core/CharacterMgr.php';
require_once 'core/ServiceMgr.php';
require_once 'core/SoapMgr.php';
require_once 'core/BonusMgr.php';

$accountMgr = new AccountMgr();
$configMgr = new ConfigMgr();
$sessionMgr = new SessionMgr();
$contentMgr = new ContentMgr();
$characterMgr = new CharacterMgr();
$serviceMgr = new ServiceMgr();
$soapMgr = new SoapMgr();
$bonusMgr = new BonusMgr();

?>