<?php

/*
Пример таблицы
CREATE TABLE `votes` (                                    
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,          
  `vote_id` int(10) unsigned NOT NULL,                    
  `time` datetime DEFAULT NULL,                           
  `ip` int(10) unsigned DEFAULT NULL,       
  `nick` varchar(20) CHARACTER SET utf8 DEFAULT NULL,     
  `votes` tinyint(4) DEFAULT NULL,                        
  PRIMARY KEY (`id`)                                      
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
*/

// Секретный ключ, такой же как в настройках сервера на рейтинге
$secret_key = 'qEdfgE398.0]2@dwd23Opd';

// возможно вам понадобится сопоставить ид мира на вашем сервере и на рейтинге
// для этого можно использовать массив
// ключ - ид игрового мира на сайте рейтинга
// значение - id вашего игрового мира
$realms = array(
    20 => 1,
);
// $realms[(int)$_POST] - вернёт ID вашего игрового мира, на вашем сервере

// Настройки подключения к БД
// Возможно у вас несколько миров и они используют несколько баз данных
// настройка подключения к базе мира с ID 1
$db_host[1] = 'localhost';
$db_user[1] = 'root';
$db_pass[1] = 'root';
$db_name[1] = 'test';

$encoding = 'utf8';
$table = 'votes';

if (isset($_POST['realm_id'], $_POST['id'], $_POST['time'], $_POST['ip'], $_POST['nickname'], $_POST['votes'], $_POST['hash'])) {
    $hash = md5($_POST['realm_id'] . $_POST['id'] . $_POST['votes'] . $_POST['nickname'] . $secret_key . $_POST['time']);
    if ($hash != $_POST['hash']) {
        // проверочный код не совпал
        // пишем логи или просто убиваем скрипт
        die();
    }
    
    try {
        // подключаемся к БД в зависимости от игрового мира
        $realm_id = (int)$_POST['realm_id'];
        $dbh = new PDO("mysql:dbname={$db_name[$realms[$realm_id]]};host={$db_host[$realms[$realm_id]]}", $db_user[$realms[$realm_id]], $db_pass[$realms[$realm_id]], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '{$encoding}'"));
        
        $fields = array(
            ':vote_id' => (int)$_POST['id'],
            ':time' =>  date('Y-m-d H:i:s', strtotime($_POST['time'])),
            ':ip' => (int)$_POST['ip'], // IP адрес передаётся 4 байтным беззнаковым числом
            ':nick' => $_POST['nickname'],
            ':votes' => (int)$_POST['votes'],
        );
        
        // заносим в таблицу информацию о голосовании
        $sth = $dbh->prepare("INSERT INTO `{$table}` (`vote_id`, `time`, `ip`, `nick`, `votes`) VALUES (:vote_id, :time, :ip, :nick, :votes)");
        if (!$sth->execute($fields)) {
            // произошла ошибка
        }
    } catch (PDOException $e) {
        echo 'Ошибка подключения: ' . $e->getMessage();
    }
}
